import TodoForm from '../components/TodoForm';
import TodoList from '../components/TodoList';
import { Container } from 'react-bootstrap';
import { useState } from 'react';

export default function Todo() {

const [todos, setTodos] = useState([]);

const addTodo = (text) => {
	const newTodo = { id: Date.now(), text, completed: false };
	setTodos([...todos, newTodo]);
}

const deleteTodo = (id) => {
	const updateTodos = todos.filter((todo) => todo.id !== id );
	setTodos(updateTodos);
}


const toggleTodo = (id) => {
	const updateTodos = todos.map((todo) => {
		if (todo.id === id ) {
			return { ...todo, completed: !todo.completed };
		}
		return todo;
	});
	setTodos(updateTodos);
}


const currentDate = new Date();
const currentDay = currentDate.toLocaleDateString('en-US', { weekday: 'long' });

	return (
		<Container>
			<div className="todo-app">
				<h1>{currentDay} Todo</h1> <hr />
				<TodoForm addTodo = {addTodo}/>
				<TodoList todos={todos} deleteTodo={deleteTodo} toggleTodo={toggleTodo} />
			</div>
		</Container>

	)
}