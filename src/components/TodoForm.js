import { useState } from 'react';

export default function TodoForm( {addTodo} ) {

const [text, setText] = useState('');

const handlSubmit = (e) => {
	e.preventDefault();
	if (text.trim() !== '' ) {
		addTodo(text);
		setText('');
	}
}


	return (
		<form onSubmit={handlSubmit}> 
			<input 
				type="text"
				placeholder="Enter a todo"
				value={text}
				onChange={(e) => setText(e.target.value)}
			/>
			<button type="submit">+</button>
		</form>
	)
}