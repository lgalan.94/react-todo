import { AiOutlineDelete } from 'react-icons/ai';

export default function TodoList( { todos, deleteTodo, toggleTodo } ) {

	return (
		<ul className = "todo-list">
			{
				todos.map((todo) => (
						<li key = {todo.id}>
							<input 
								type="checkbox"
								checked={todo.completed}
								onChange={() => toggleTodo(todo.id)}
							/>
							<span className={todo.completed ? 'completed' : '' } > { todo.text } </span>
							<button className="" onClick={() => deleteTodo(todo.id)}> <AiOutlineDelete /> </button>
						</li>
					))
			}
		</ul>
	)
}