import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Todo from './pages/Todo';

function App() {
  return (
    <>
      <Todo />
    </>
  );
}

export default App;
